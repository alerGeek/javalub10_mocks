package com.demo.camera;

public class PhotoCamera implements WriteListener {

    private ImageSensor sensor;
    private Card card;

    private boolean isTurnedOn = false;
    private boolean isBeforeSaving = true;
    private boolean isSavingCompleted = false;

    public PhotoCamera(ImageSensor sensor, Card card) {
        this.sensor = sensor;
        this.card = card;
    }

    public void turnOn() {
        sensor.turnOn();
        isTurnedOn = true;
    }

    public void turnOff() {
        if(isSavingCompleted || isBeforeSaving){
            sensor.turnOff();
            isTurnedOn = false;
        }
        isTurnedOn = false;
    }

    public void pressButton() {
        if (isTurnedOn) {
            card.write(sensor.read());
            isBeforeSaving = false;
        }
    }

    @Override
    public void writeCompleted() {
        isSavingCompleted = true;
        sensor.turnOff();
    }
}

