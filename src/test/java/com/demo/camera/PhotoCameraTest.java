package com.demo.camera;

import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;

public class PhotoCameraTest {



    @Test
    public void turningOnCameraShouldTurnOnSensor() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor, card);

        camera.turnOn();

        Mockito.verify(sensor).turnOn();
    }

    @Test

    public void turningOffCameraShouldTurnOffSensor() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor, card);

        camera.turnOff();

        Mockito.verify(sensor).turnOff();
    }

    @Test
    public void pressingButtonShouldNotWorkWhenCameraIsTurnedOff() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor,card);

        camera.pressButton();

        Mockito.verifyZeroInteractions(sensor);
    }

    @Test
    public void pressingButtonShouldCopyDataFromSensorToCardWhenCameraIsOn(){
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor,card);

        camera.turnOn();
        camera.pressButton();

        Mockito.verify(card).write(sensor.read());
    }

    @Test
    public void whileSavingDataTurningOffCameraDoesntTurnOffSensor(){
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor,card);

        camera.turnOn();
        camera.pressButton();
        Mockito.clearInvocations(sensor);
        camera.turnOff();
        Mockito.verifyZeroInteractions(sensor);
    }

    @Test
    public void afterSavingDataSensorShouldTurnOff(){
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor,card);

        camera.turnOn();
        camera.pressButton();
        camera.writeCompleted();

        Mockito.verify(sensor).turnOff();
    }

}
